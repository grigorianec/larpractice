<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//use App\Http\Middleware\FilledProfile;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');

//Route::get('/', function (){
//	return view('welcome');
//});

Auth::routes();

Route::get('/cabinet', 'Cabinet\HomeController@index')->name('cabinet');

//Route::get('/cabinet', 'Auth\LoginController@index')->name('login');

Route::get('/verify/{token})', 'Auth\RegisterController@verify')->name('register.verify');

//Route::get('/cabinet', 'Cabinet\HomeController@index')->name('cabinet.home');

Route::group(
	[
		'prefix' => 'cabinet',
		'as' => 'cabinet.',
		'namespace' => 'Cabinet',
		'middleware' => ['auth'],
	],
	function () {
		Route::get('/', 'HomeController@index')->name('home');

//		Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
//			Route::get('/', 'ProfileController@index')->name('home');
//			Route::get('/edit', 'ProfileController@edit')->name('edit');
//			Route::put('/update', 'ProfileController@update')->name('update');
//			Route::post('/phone', 'PhoneController@request');
//			Route::get('/phone', 'PhoneController@form')->name('phone');
//			Route::put('/phone', 'PhoneController@verify')->name('phone.verify');
//
//			Route::post('/phone/auth', 'PhoneController@auth')->name('phone.auth');
//		});
//
//		Route::get('favorites', 'FavoriteController@index')->name('favorites.index');
//		Route::delete('favorites/{advert}', 'FavoriteController@remove')->name('favorites.remove');
//
//		Route::resource('tickets', 'TicketController')->only(['index', 'show', 'create', 'store', 'destroy']);
//		Route::post('tickets/{ticket}/message', 'TicketController@message')->name('tickets.message');

//		Route::group([
//			'prefix' => 'adverts',
//			'as' => 'adverts.',
//			'namespace' => 'Adverts',
//			'middleware' => [App\Http\Middleware\FilledProfile::class],
//		], function () {
//			Route::get('/', 'AdvertController@index')->name('index');
//			Route::get('/create', 'CreateController@category')->name('create');
//			Route::get('/create/region/{category}/{region?}', 'CreateController@region')->name('create.region');
//			Route::get('/create/advert/{category}/{region?}', 'CreateController@advert')->name('create.advert');
//			Route::post('/create/advert/{category}/{region?}', 'CreateController@store')->name('create.advert.store');
//
//			Route::get('/{advert}/edit', 'ManageController@editForm')->name('edit');
//			Route::put('/{advert}/edit', 'ManageController@edit');
//			Route::get('/{advert}/photos', 'ManageController@photosForm')->name('photos');
//			Route::post('/{advert}/photos', 'ManageController@photos');
//			Route::get('/{advert}/attributes', 'ManageController@attributesForm')->name('attributes');
//			Route::post('/{advert}/attributes', 'ManageController@attributes');
//			Route::post('/{advert}/send', 'ManageController@send')->name('send');
//			Route::post('/{advert}/close', 'ManageController@close')->name('close');
//			Route::delete('/{advert}/destroy', 'ManageController@destroy')->name('destroy');
		});

Route::group(
	[
		'prefix' => 'admin',
		'as' => 'admin.',
		'namespace' => 'Admin',
		'middleware' => ['auth', 'can:admin-panel'],
	],
	function () {
//		Route::post( '/ajax/upload/image', 'UploadController@image' )->name( 'ajax.upload.image' );

		Route::get( '/', 'HomeController@index' )->name( 'home' );
		Route::resource( 'users', 'UserController' );
		Route::get( '/users/{user}/roles', 'UserController@changeRoleView' )->name( 'user.roles' );
		Route::post( '/users/{user}/roles-edit', 'UserController@changeRoleEdit' )->name( 'user.roles-edit' );
//		Route::post( '/users/{user}/verify', 'UsersController@verify' )->name( 'users.verify' );

		Route::resource( 'regions', 'RegionController' );
		Route::group(['prefix' => 'adverts', 'as' => 'adverts.', 'namespace' => 'Adverts'], function () {

			Route::resource( 'categories', 'CategoryController' );

			Route::group(['prefix' => 'categories/{category}', 'as' => 'categories.'], function () {
				Route::post('/first', 'CategoryController@first')->name('first');
				Route::post('/up', 'CategoryController@up')->name('up');
				Route::post('/down', 'CategoryController@down')->name('down');
				Route::post('/last', 'CategoryController@last')->name('last');
				Route::resource('attributes', 'AttributeController')->except('index');
			});
		});
//
//		Route::resource( 'pages', 'PageController' );
//
//		Route::group( [ 'prefix' => 'pages/{page}', 'as' => 'pages.' ], function () {
//			Route::post( '/first', 'PageController@first' )->name( 'first' );
//			Route::post( '/up', 'PageController@up' )->name( 'up' );
//			Route::post( '/down', 'PageController@down' )->name( 'down' );
//			Route::post( '/last', 'PageController@last' )->name( 'last' );
//		} );
	});


