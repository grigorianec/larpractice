<?php

use App\Entity\Region;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */

$factory->define( Region::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->city,
        'slug' => $faker->unique()->slug(2),
//        'name' => $name = $faker->unique()->city,
//        'slug' => str_slug($name),
        'parent_id' => null,
    ];
});
