<?php

use App\Entity\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//	    factory(User::class, 30)->create();
	    $this->call(RegionsTableSeeder::class);
	    $this->call(AdvertCategoriesTableSeeder::class);
    }
}
