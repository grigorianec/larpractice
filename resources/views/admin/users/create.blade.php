@extends('layouts.app')

@section('content')
    @include('admin.users._nav')

    <form method="POST" action="{{ route('admin.users.store') }}">
        @csrf

        <div class="form-group">
            <label for="name" class="col-form-label">Name</label>
            <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="email" class="col-form-label">E-Mail Address</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
                <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
        <div class="form-group pt-2">
            <select class="custom-select{{ $errors->has('status') ? ' is-invalid' : '' }}" id="status" name="status" required>
                <option selected>Выберите статус</option>
                <option value="active">active</option>
                <option value="wait">wait</option>
            </select>
            @if ($errors->has('status'))
                <span class="invalid-feedback"><strong>{{ $errors->first('status') }}</strong></span>
            @endif
        </div>
{{--        <div class="form-group pt-2">--}}
{{--            <select class="custom-select {{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" id="level" required>--}}
{{--                <option selected>Выберите доступ</option>--}}
{{--                <option value="ROLE_USER">User</option>--}}
{{--                <option value="ROLE_ADMIN">Admin</option>--}}
{{--                <option value="ROLE_MODERATOR">Moderator</option>--}}
{{--            </select>--}}
{{--            @if ($errors->has('role'))--}}
{{--                <span class="invalid-feedback"><strong>{{ $errors->first('role') }}</strong></span>--}}
{{--            @endif--}}
{{--        </div>--}}
        <div class="form-group">
            <label for="password" class="col-form-label">Password</label>
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  required>
            @if ($errors->has('password'))
                <span class="invalid-feedback"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection