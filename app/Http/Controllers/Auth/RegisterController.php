<?php

namespace App\Http\Controllers\Auth;

use App\Mail\Auth\VerifyMail;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/cabinet';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
	        'verify_token' => Str::uuid(),
	        'status' => User::STATUS_WAIT,
	        'role' => [User::ROLE_USER]
        ]);

		Mail::to($user->email)->send(new VerifyMail($user));
		event(new Registered($user));

		return redirect()->route('login')
			->with('success', 'Check your email and click on the link to verify');
    }

    protected function registered( Request $request, $user )
    {
    	$this->guard()->logout();

    	return redirect()->route('login')
	                            ->with('success', 'Check your email and click on the link to verify');
    }

	public function verify($token)
	{
		if (!$user = User::where('verify_token', $token)->first()) {
			return redirect()->route('login')
			                 ->with('error', 'Sorry your link cannot be identified.');
		}

		if (!$user->status !== User::STATUS_WAIT){
			$this->guard()->logout();
			return back()->with('error', 'Your e-mail is already verified');
		}

		$user->status = User::STATUS_ACTIVE;
		$user->verify_token = null;
		$user->save();
		return redirect()->route('login')->with('success', 'Your e-mail is verified. You can now login.');

	}

//    public function register( RegisterRequest $request )
//    {
//    	if ($request->isMethod('POST')){
//
//    		$this->validate($request, [
//    			'name' => 'required|string|max:255',
//			    'email' => 'required|string|email|max:255|unique:users',
//			    'password' => 'required|string|min:6|confirmed'
//		    ]);
//
//		    $user = User::create([
//			    'name' => $request['name'],
//			    'email' => $request['email'],
//			    'password' => Hash::make($request['password']),
//		    ]);
//		    Auth::login($user);
//		    return $this->redirectTo()->route('home');
//	    }
//    	return view('auth.register');
//    }

//	public function form()
//	{
//		return view('auth.register');
//	}
//
//	public function register( Request $request )
//	{
//
//		$this->validate($request, [
//			'name' => 'required|string|max:255',
//			'email' => 'required|string|email|max:255|unique:users',
//			'password' => 'required|string|min:6|confirmed'
//		]);
//		if(!true){
//			return $this->redirect()->route('form')->exceptInput();
//		}
//
//		$user = User::create([
//			'name' => $request['name'],
//			'email' => $request['email'],
//			'password' => Hash::make($request['password']),
//		]);
//		Auth::login($user);
//
//		return $this->redirect()->route('cabinet');
//	}
}
