<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Entity\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    use ThrottlesLogins;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/cabinet';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated( Request $request, $user )
    {
    	if ($user->status !== User::STATUS_ACTIVE){
    		$this->guard()->logout();
    		return back()->with('error', 'You need to confirm your account. Please check your email');
	    }
    	return redirect()->intended($this->redirectPath());
    }

}
