<?php

namespace App\Http\Controllers\Admin;

use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateRequest;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{

	/**
	 * @var CheckUserRole
	 */
	private $checkUserRole;

	public function __construct()
	{
//		$this->middleware('can:admin-panel');
//		if ($this->checkUserRole->checkRole('ROLE_ADMIN') !== true){
//			abort(403);
//			die();
//		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
    public function index(Request $request)
    {
//    	if (Auth::user()->checkRole('ROLE_ADMIN') !== true){
//    		return abort(403);
//	    }
	    $query = User::orderByDesc('id');

	    if (!empty($value = $request->get('id'))) {
		    $query->where('id', $value);
	    }

	    if (!empty($value = $request->get('name'))) {
		    $query->where('name', 'like', '%' . $value . '%');
	    }

	    if (!empty($value = $request->get('email'))) {
		    $query->where('email', 'like', '%' . $value . '%');
	    }

	    if (!empty($value = $request->get('status'))) {
		    $query->where('status', $value);
	    }

	    if (!empty($value = $request->get('role'))) {
		    $query->where('role', 'like', '%' . $value . '%');
	    }

	    $users = $query->paginate(20);

	    $statuses = [
		    User::STATUS_WAIT => 'wait',
		    User::STATUS_ACTIVE => 'active',
	    ];

	    $roles = User::rolesList();

	    return view('admin.users.index', compact('users','statuses', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

	    return view('admin.users.create');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateRequest $request
	 *
	 * @return void
	 */
    public function store(CreateRequest $request)
    {

	    $user = User::new(
		    $request['name'],
		    $request['email'],
		    $request['status'],
		    $request['password']
	    );

	    return redirect()->route('admin.users.show', $user);
    }

	/**
	 * Display the specified resource.
	 *
	 * @param User $user
	 *
	 * @return Response
	 */
    public function show(User $user)
    {
	    return view('admin.users.show', compact('user'));
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param User $user
	 *
	 * @return Response
	 */
    public function edit(User $user)
    {
	    return view('admin.users.edit', compact('user'));
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param CreateRequest $request
	 * @param User $user
	 *
	 * @return Response
	 */
    public function update(CreateRequest $request, User $user)
    {
	    $user->update($request->only(['name', 'email']));

	    return redirect()->route('admin.users.show', $user);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param User $user
	 *
	 * @return Response
	 * @throws Exception
	 */
	public function destroy(User $user)
	{
		$user->delete();

		return redirect()->route('admin.users.index');
	}

	/**
	 * @param User $user
	 *
	 * @return RedirectResponse
	 */
	public function changeRoleView(User $user)
	{
		$roles = User::rolesList();
		return view('admin.users.roles', [
			'user' => $user,
			'roles' => $roles
		]);
	}

	/**
	 * @return RedirectResponse
	 */
	public function changeRoleEdit(User $user, Request $request)
	{
		$user->update([
			'role' => $request->role
		]);

		return redirect()->route('admin.users.index');
	}
}
