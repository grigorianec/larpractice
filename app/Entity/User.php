<?php

namespace App\Entity;

use Carbon\Carbon;
use DomainException;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @property int $id
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property bool $phone_verified
 * @property string $password
 * @property string $verify_token
 * @property string $phone_verify_token
 * @property Carbon $phone_verify_token_expire
 * @property boolean $phone_auth
 * @property array $role
 * @property string $status
 *
 */

class User extends Authenticatable
{
    use Notifiable;

	public const STATUS_WAIT = 'wait';
	public const STATUS_ACTIVE = 'active';

	public const ROLE_USER = "ROLE_USER";
	public const ROLE_MODERATOR = "ROLE_MODERATOR";
	public const ROLE_ADMIN = "ROLE_ADMIN";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','status', 'role', 'password','verify_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
	    'role' => 'array'
    ];

	public static function new($name, $email, $status, $password): self
	{
		return static::create([
			'name' => $name,
			'email' => $email,
			'status' => $status,
			'verify_token' => Str::uuid(),
			'role' => [self::ROLE_USER],
			'password' => Hash::make($password)
		]);
	}

	public static function rolesList(): array
	{
		return [
			self::ROLE_USER => "ROLE_USER",
			self::ROLE_MODERATOR => "ROLE_MODERATOR",
			self::ROLE_ADMIN => "ROLE_ADMIN",
		];
	}

	public function isWait(): bool
	{
		return $this->status === self::STATUS_WAIT;
	}

	public function isActive(): bool
	{
		return $this->status === self::STATUS_ACTIVE;
	}

	public function verify(): void
	{
		if (!$this->isWait()) {
			throw new DomainException('User is already verified.');
		}

		$this->update([
			'status' => self::STATUS_ACTIVE,
			'verify_token' => null,
		]);
	}

	public function changeRole($role): void
	{
		if (!array_key_exists($role, self::rolesList())) {
			throw new \InvalidArgumentException('Undefined role "' . $role . '"');
		}
		if ($this->role === $role) {
			throw new DomainException('Role is already assigned.');
		}
		$this->update(['role' => $role]);
	}

	public function isModerator(): bool
	{
		return $this->checkRole(self::ROLE_MODERATOR);
	}

	public function isAdmin(): bool
	{
		return $this->checkRole(self::ROLE_ADMIN);
	}

	public function isUser(): bool
	{
		return $this->checkRole(self::ROLE_USER);
	}

	public function checkRole($roleName):bool
	{
		foreach ($this->role as $rol){
			if ($roleName === $rol){
				return true;
			}
		}
		return false;
	}

}
