<?php

namespace App\Console\Commands\User;

use App\Entity\User;
use App\Service\Auth\RegisterService;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VerifyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:verify {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for user verification through email';
	/**
	 * @var RegisterService
	 */
	private $service;

	/**
	 * Create a new command instance.
	 *
	 * @param RegisterService $service
	 */
    public function __construct(RegisterService $service)
    {
        parent::__construct();
	    $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
//    public function handle(): bool
//    {
//        $this->info('Success');
//        return true;
//    }

	//For symfony
//	protected function execute( InputInterface $input, OutputInterface $output ) {
//		return $output->writeln('<info>Success</info>');
//	}

	public function handle(): bool
	{
		$email = $this->argument('email');

		if (!$user = User::where('email', $email)->first()) {
			$this->error('Undefined user with email ' . $email);
			return false;
		}

		try{
			$this->service->verify($user->id);
		}catch (\DomainException $e) {
			$this->error($e->getMessage());
			return false;
		}

		$this->info('User is successfully verified');
		return true;
	}
}
